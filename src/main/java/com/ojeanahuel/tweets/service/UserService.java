package com.ojeanahuel.tweets.service;

import com.ojeanahuel.tweets.dto.UserDTO;
import com.ojeanahuel.tweets.entity.User;
import com.ojeanahuel.tweets.exception.ForbiddenException;
import com.ojeanahuel.tweets.mapper.UserMapper;
import com.ojeanahuel.tweets.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class UserService {

    @Autowired private UserRepository userRepository;
    @Autowired private UserMapper userMapper;

    public UserDTO getUser(UUID idUser) {
        if (idUser == null){
            throw new ForbiddenException("IdUser can not be null");
        }

        Optional<User> optional = userRepository.findById(idUser);
        if (optional.isPresent()){
            User user = optional.get();
            return userMapper.toDto(user);
        } else {
            throw new ForbiddenException("There is no tweet with that id");
        }
    }

    public UserDTO addUser(UserDTO userDTO) {

        if (userDTO.getIdUser() == null){
            throw new ForbiddenException("IdUser can not be null");
        }

        if (userDTO.getName() == null || userDTO.getName().isEmpty()){
            throw new ForbiddenException("Name can not be empty");
        }

        if (userDTO.getFollowers() == null){
            throw new ForbiddenException("Followers can not be null");
        }

        userRepository.save(userMapper.toDomain(userDTO));

        return userDTO;
    }
}
