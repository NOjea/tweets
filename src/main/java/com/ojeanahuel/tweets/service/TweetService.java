package com.ojeanahuel.tweets.service;

import com.ojeanahuel.tweets.datatype.Language;
import com.ojeanahuel.tweets.dto.HashTagDTO;
import com.ojeanahuel.tweets.dto.TweetDTO;
import com.ojeanahuel.tweets.dto.UserDTO;
import com.ojeanahuel.tweets.entity.Tweet;
import com.ojeanahuel.tweets.exception.ForbiddenException;
import com.ojeanahuel.tweets.mapper.TweetMapper;
import com.ojeanahuel.tweets.repository.TweetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class TweetService {

    @Value("${tweets.min-followers:1500}") private Long minFollowers;
    @Value("${tweets.hashtags-ranking-size:10}") private Long hashTagsRankingSize;

    @Autowired private TweetRepository tweetRepository;
    @Autowired private TweetMapper tweetMapper;

    @Autowired private UserService userService;

    public TweetDTO addTweet (TweetDTO tweetDTO){

        if (tweetDTO == null){
            throw new ForbiddenException("TweetDTO can not be null");
        }

        if (tweetDTO.getIdTweet() == null){
            throw new ForbiddenException("IdTweet can not be null");
        }

        if (Language.getLanguage(tweetDTO.getLocalization()) == null){
            throw new ForbiddenException("Language error");
        }

        UserDTO userDTO = userService.getUser(tweetDTO.getUserId());
        if (userDTO == null){
            throw new ForbiddenException("The user does not exist");
        }

        if (userDTO.getFollowers() < minFollowers){
            throw new ForbiddenException("the user does not have the minimum number of followers");
        }

        tweetRepository.save(tweetMapper.toDomain(tweetDTO));
        return tweetDTO;
    }

    public void validateTweet(UUID idTweet) {

        if (idTweet == null){
            throw new ForbiddenException("IdTweet can not be null");
        }

        Optional<Tweet> optional = tweetRepository.findById(idTweet);
        if (optional.isPresent()){
            Tweet tweet = optional.get();
            tweet.setValid(true);
            tweetRepository.save(tweet);
        } else {
            throw new ForbiddenException("There is no tweet with that id");
        }
    }

    public List<TweetDTO> getTweets(Boolean validTweets) {

        List<Tweet> tweets;
        if (validTweets){
            tweets = tweetRepository.findByIsValidTrue();
        } else {
            tweets = tweetRepository.findAll();
        }
        if (tweets != null){
            return tweetMapper.toLstDto(tweets);
        }
        return new ArrayList<>();
    }

    public List<HashTagDTO> getHashTagsRanking() {
        List<HashTagDTO> hashTagDTOs = new ArrayList<>();
        List<Object[]> hashTagsRanking = tweetRepository.getHashTagsRanking(hashTagsRankingSize);
        for (Object[] object : hashTagsRanking) {
            HashTagDTO hashTagDTO = new HashTagDTO(object[0].toString(), new BigInteger(object[1].toString()));
            hashTagDTOs.add(hashTagDTO);
        }
        return hashTagDTOs;
    }
}
