package com.ojeanahuel.tweets.datatype;

public enum Language {
    SPANISH("es"),
    FRENCH("fr"),
    ITALIAN("it");

    private String value;

    Language(String value) {
        this.value = value;
    }

    public static Language getLanguage (String value){
        switch (value){
            case "es":
                return SPANISH;
            case "fr":
                return FRENCH;
            case "it":
                return ITALIAN;
            default:
                return null;
        }
    }

    public String getValue() {
        return value;
    }
}
