package com.ojeanahuel.tweets.entity;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "tweets")
public class Tweet {

    @Id
    @Column(name = "idtweet")
    private UUID idTweet;

    @Column(name = "text")
    private String text;

    @Column(name = "localization")
    private String localization;

    @Column(name = "isvalid")
    private Boolean isValid;

    @ManyToOne(fetch = FetchType.LAZY , cascade= CascadeType.ALL)
    @JoinColumns({
            @JoinColumn(name = "userid")
    })
    private User user;

    public UUID getIdTweet() {
        return idTweet;
    }

    public void setIdTweet(UUID idTweet) {
        this.idTweet = idTweet;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getLocalization() {
        return localization;
    }

    public void setLocalization(String localization) {
        this.localization = localization;
    }

    public Boolean getValid() {
        return isValid;
    }

    public void setValid(Boolean valid) {
        isValid = valid;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
