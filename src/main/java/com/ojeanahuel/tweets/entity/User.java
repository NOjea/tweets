package com.ojeanahuel.tweets.entity;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "users")
public class User {

    @Id
    @Column(name = "iduser")
    private UUID idUser;

    @Column(name = "name")
    private String name;

    @Column(name = "followers")
    private Integer followers;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name ="userid", insertable = false, updatable = false)
    })
    private List<Tweet> lstTweets;

    public UUID getIdUser() {
        return idUser;
    }

    public void setIdUser(UUID idUser) {
        this.idUser = idUser;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getFollowers() {
        return followers;
    }

    public void setFollowers(Integer followers) {
        this.followers = followers;
    }

    public List<Tweet> getLstTweets() {
        return lstTweets;
    }

    public void setLstTweets(List<Tweet> lstTweets) {
        this.lstTweets = lstTweets;
    }
}
