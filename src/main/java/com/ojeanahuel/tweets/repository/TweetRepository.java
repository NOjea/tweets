package com.ojeanahuel.tweets.repository;

import com.ojeanahuel.tweets.dto.HashTagDTO;
import com.ojeanahuel.tweets.entity.Tweet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface TweetRepository extends JpaRepository<Tweet, UUID> {

    List<Tweet> findByIsValidTrue ();

    @Query(value = "SELECT word as word, count(*) as count\n" +
            "FROM ( \n" +
            "  SELECT regexp_split_to_table(text, '\\s') as word\n" +
            "  FROM tweets\n" +
            ") t\n" +
            "where SUBSTRING( word, 1, 1 ) = '#'\n" +
            "GROUP BY word\n" +
            "order by count desc\n" +
            "limit ?1", nativeQuery = true)
    List<Object[]> getHashTagsRanking(Long limit);
}
