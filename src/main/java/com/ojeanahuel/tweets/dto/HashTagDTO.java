package com.ojeanahuel.tweets.dto;

import java.math.BigInteger;

public class HashTagDTO {

    private String word;
    private BigInteger count;

    public HashTagDTO(String word, BigInteger count) {
        this.word = word;
        this.count = count;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public BigInteger getCount() {
        return count;
    }

    public void setCount(BigInteger count) {
        this.count = count;
    }
}
