package com.ojeanahuel.tweets.dto;

import java.util.UUID;

public class TweetDTO {

    private UUID idTweet;
    private String text;
    private String localization;
    private Boolean isValid;
    private UUID userId;

    public UUID getIdTweet() {
        return idTweet;
    }

    public void setIdTweet(UUID idTweet) {
        this.idTweet = idTweet;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getLocalization() {
        return localization;
    }

    public void setLocalization(String localization) {
        this.localization = localization;
    }

    public Boolean getValid() {
        return isValid;
    }

    public void setValid(Boolean valid) {
        isValid = valid;
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }
}
