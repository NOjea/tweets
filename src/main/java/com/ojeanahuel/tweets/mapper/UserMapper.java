package com.ojeanahuel.tweets.mapper;

import com.ojeanahuel.tweets.dto.UserDTO;
import com.ojeanahuel.tweets.entity.User;
import org.springframework.stereotype.Component;

@Component
public class UserMapper extends GenericMapperImpl<User, UserDTO>{

    @Override
    public UserDTO toDto(User domain) {
        UserDTO userDTO = new UserDTO();
        userDTO.setIdUser(domain.getIdUser());
        userDTO.setName(domain.getName());
        userDTO.setFollowers(domain.getFollowers());
        return userDTO;
    }

    @Override
    public User toDomain(UserDTO dto) {
        User user = new User();
        user.setIdUser(dto.getIdUser());
        user.setName(dto.getName());
        user.setFollowers(dto.getFollowers());
        return user;
    }
}
