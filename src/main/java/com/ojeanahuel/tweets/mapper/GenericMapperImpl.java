package com.ojeanahuel.tweets.mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public abstract class GenericMapperImpl<T, U> implements GenericMapper<T, U> {

	public List<T> toLstDomain(List<U> lstDto) {
		if (lstDto != null) {
			return lstDto.stream()
					.map(dto -> toDomain(dto))
					.collect(Collectors.toList());
		} else {
			return new ArrayList<>();
		}
	}

	public List<U> toLstDto(List<T> lstDomain) {
		if (lstDomain != null) {
			return lstDomain.stream()
					.map(domain -> toDto(domain))
					.collect(Collectors.toList());
		} else {
			return new ArrayList<>();
		}
	}
}