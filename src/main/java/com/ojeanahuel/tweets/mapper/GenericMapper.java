package com.ojeanahuel.tweets.mapper;

public interface GenericMapper<T, U> {

	U toDto(T domain);
	
	T toDomain(U dto);
	
}
