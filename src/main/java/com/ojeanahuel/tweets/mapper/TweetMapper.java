package com.ojeanahuel.tweets.mapper;

import com.ojeanahuel.tweets.dto.TweetDTO;
import com.ojeanahuel.tweets.entity.Tweet;
import com.ojeanahuel.tweets.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TweetMapper extends GenericMapperImpl<Tweet, TweetDTO>{

    @Autowired private UserRepository userRepository;

    @Override
    public TweetDTO toDto(Tweet domain) {
        TweetDTO tweetDTO = new TweetDTO();
        tweetDTO.setIdTweet(domain.getIdTweet());
        tweetDTO.setUserId(domain.getUser().getIdUser());
        tweetDTO.setLocalization(domain.getLocalization());
        tweetDTO.setText(domain.getText());
        tweetDTO.setValid(domain.getValid());
        return tweetDTO;
    }

    @Override
    public Tweet toDomain(TweetDTO dto) {
        Tweet tweet = new Tweet();
        tweet.setIdTweet(dto.getIdTweet());
        tweet.setUser(userRepository.getOne(dto.getUserId()));
        tweet.setLocalization(dto.getLocalization());
        tweet.setText(dto.getText());
        tweet.setValid(dto.getValid());
        return tweet;
    }
}
