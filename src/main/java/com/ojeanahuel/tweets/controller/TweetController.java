package com.ojeanahuel.tweets.controller;

import com.ojeanahuel.tweets.dto.HashTagDTO;
import com.ojeanahuel.tweets.dto.TweetDTO;
import com.ojeanahuel.tweets.exception.ForbiddenException;
import com.ojeanahuel.tweets.exception.InternalServerErrorException;
import com.ojeanahuel.tweets.service.TweetService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@Api(value = "Tweets service", description = "This service has a CRUD for tweets")
@RestController
@RequestMapping("/tweets")
public class TweetController {

    private static final Logger log = LogManager.getLogger(TweetController.class);

    @Autowired private TweetService tweetService;

    @ApiOperation(value = "Validate tweet", notes = "Validate tweet by id")
    @PatchMapping("/validate")
    public ResponseEntity<HttpStatus> validateTweet (@RequestBody UUID idTweet){
        try{
            tweetService.validateTweet( idTweet );
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (ForbiddenException ex){
            log.error("The tweet could not be validate correctly: " + ex.getMessage(), ex);
            throw new ForbiddenException(ex.getMessage());
        } catch (Exception ex){
            log.error("The tweet could not be validate correctly: " + ex.getMessage(), ex);
            throw new InternalServerErrorException("The tweet could not be validate correctly");
        }
    }

    @ApiOperation(value = "Add tweet", notes = "Return the saved tweet" )
    @PutMapping("/add")
    public ResponseEntity<HttpStatus> addTweet (@RequestBody TweetDTO tweetDTO){
        try{
            tweetService.addTweet( tweetDTO );
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (ForbiddenException ex){
            log.error("The tweet could not be added correctly: " + ex.getMessage(), ex);
            throw new ForbiddenException(ex.getMessage());
        } catch (Exception ex){
            log.error("The tweet could not be added correctly: " + ex.getMessage(), ex);
            throw new InternalServerErrorException("The tweet could not be added correctly");
        }
    }

    @ApiOperation(value = "All tweets", notes = "Return all tweets" )
    @GetMapping("/all")
    public ResponseEntity<List<TweetDTO>> getTweets (){
        try{
            List<TweetDTO> tweetDTOList = tweetService.getTweets(false);
            return new ResponseEntity<>(tweetDTOList, HttpStatus.OK);
        } catch (ForbiddenException ex){
            log.error("The tweets could not be obtained correctly: " + ex.getMessage(), ex);
            throw new ForbiddenException(ex.getMessage());
        } catch (Exception ex){
            log.error("The tweets could not be obtained correctly: " + ex.getMessage(), ex);
            throw new InternalServerErrorException("The tweets could not be obtained correctly");
        }
    }

    @ApiOperation(value = "Valid tweets", notes = "Return valid tweets" )
    @GetMapping("/valids")
    public ResponseEntity<List<TweetDTO>> getValidTweets (){
        try{
            List<TweetDTO> tweetDTOList = tweetService.getTweets(true);
            return new ResponseEntity<>(tweetDTOList, HttpStatus.OK);
        } catch (ForbiddenException ex){
            log.error("The tweets could not be obtained correctly: " + ex.getMessage(), ex);
            throw new ForbiddenException(ex.getMessage());
        } catch (Exception ex){
            log.error("The tweets could not be obtained correctly: " + ex.getMessage(), ex);
            throw new InternalServerErrorException("The tweets could not be obtained correctly");
        }
    }

    @ApiOperation(value = "Hashtags ranking", notes = "Returns the most used hashtags sorted by number of uses" )
    @GetMapping("/hashTagsRanking")
    public ResponseEntity<List<HashTagDTO>> getHashTagsRanking (){
        try{
            List<HashTagDTO> hashTagDTOList = tweetService.getHashTagsRanking();
            return new ResponseEntity<>(hashTagDTOList, HttpStatus.OK);
        } catch (ForbiddenException ex){
            log.error("The HashTags Ranking could not be obtained correctly: " + ex.getMessage(), ex);
            throw new ForbiddenException(ex.getMessage());
        } catch (Exception ex){
            log.error("The HashTags Ranking could not be obtained correctly: " + ex.getMessage(), ex);
            throw new InternalServerErrorException("The HashTags Ranking could not be obtained correctly");
        }
    }
}
