package com.ojeanahuel.tweets.controller;

import com.ojeanahuel.tweets.dto.UserDTO;
import com.ojeanahuel.tweets.exception.ForbiddenException;
import com.ojeanahuel.tweets.exception.InternalServerErrorException;
import com.ojeanahuel.tweets.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "Users service", description = "This service has a CRUD for users")
@RestController
@RequestMapping("/users")
public class UserController {

    private static final Logger log = LogManager.getLogger(UserController.class);

    @Autowired private UserService userService;

    @ApiOperation(value = "Add user", notes = "Return the saved user" )
    @PutMapping("/add")
    public ResponseEntity<HttpStatus> addUser (@RequestBody UserDTO userDTO){
        try{
            userService.addUser( userDTO );
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (ForbiddenException ex){
            log.error("The user could not be added correctly: " + ex.getMessage(), ex);
            throw new ForbiddenException(ex.getMessage());
        } catch (Exception ex){
            log.error("The user could not be added correctly: " + ex.getMessage(), ex);
            throw new InternalServerErrorException("The user could not be added correctly");
        }
    }
}
